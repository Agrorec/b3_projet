# Installation et configuration d'Asterisk
## Installation

Premièrement on met à jour le système avec la commande suivante :
```bash
sudo apt update
```

Par la suite on installe les paquets nécessaire :

```bash
sudo apt install asterisk asterisk-dahdi
```

A présent on active le service et on l'allume en même temps en une commande :

```bash
sudo systemctl enable --now asterisk
```
On peut vérifier qu'il est bien actif avec la commande : 

```bash
sudo systemctl status asterisk
```

## Configuration d'Asterisk

Premièrement on se rend dans le dossier qui contient les fichiers de configuration d'asterisk
```bash
cd /etc/asterisk
```

Puis on renomme les fichiers sip.conf et extensions.conf

```bash
sudo mv extensions.conf extensions.conf.sample

sudo mv sip.conf sip.conf.sample
```

Enfin, on va taper quelques lignes de conf
Prenons le cas où nous souhaitons créer deux profils pour que deux personnes puissent s'appeler.

On commencer par modifier le fichier **sip.conf** qui est responsable de la configurations des options du protocole SIP pour asterisk
(Session Initiation Protocol, abrégé SIP, est un protocole de communication standard ouvert de gestion de sessions souvent utilisé dans les télécommunications multimédia (son, image, etc.) source : Wikipédia)

```bash
vim sip.conf

[general]
context=default
nat = yes

[1001]
type=friend
username = vlachamp
fullname = Valentin LACHAMP
context=from-internal
host=dynamic
dtmfmode=rfc2833
secret=password
disallow=all
allow=ulaw

[1002]
type=friend
username = cdournet
fullname = Clement DOURNET
context=from-internal
host=dynamic
dtmfmode=rfc2833
secret=password
disallow=all
allow=ulaw
```

Avec ce fichier nous avons donc deux utilisateurs identifié par deux numéro : **1001** et **1002**

&nbsp;

Par la suite on modifie le fichier **extensions.conf** :

```bash
[from-internal]

exten=>1001,1,Dial(SIP/1001,20)
exten=>1002,1,Dial(SIP/1002,20)
```

Ce fichier permet de dire quoi faire si un appel est reçu. 

On peut lire ces lignes de la manière suivante :
exten : Déclare le numéro
1001 : Numéro composé
1 : Ordre de l'extension
Dial : Application qui va être utilisé
SIP : Protocol qui va être utilisé
20 : Temps d'attente avant de passer à la prochaine étape

&nbsp;

A présent on télécharge l'application **Linphone** sur son ordinateur ou son smartphone

![](https://www.how2shout.com/linux/wp-content/uploads/2021/11/Login-Android-SIP-using-Android.png)

Et on s'authentifie avec son numéro qui équivaut au username, son mot de passe prédéfini dans le fichier de configuration

Puis on passe notre premier appel en appelant 1001 : 
![](https://www.how2shout.com/linux/wp-content/uploads/2021/11/SIP-Asterisk-call-Debian-11-Bullseye.png)

Et surtout on dit le mot magique

![](https://c.tenor.com/pnKJSOpF3eAAAAAC/vilebrequin-vilebrequin-sylvain.gif)
