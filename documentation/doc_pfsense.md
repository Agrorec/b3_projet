# PfSense

## Diverses configurations

### Redirections de ports

Afin de permettre au RemoteApp de fonctionner, il est nécessaire de rediriger deux ports : 443 pour l'accès à l'interface web et 3389 pour l'accès aux RemoteApp. Pour cela, il faut se connecter sur l'interface web du pfsense. Une fois connecté sur l'interface web, il faut aller dans l'onglet `firewall`, choisir `NAT`. L'interface est `WAN`, de type IPv4 et TCP. La destination est une `WAn address` avec comme ports `HTTPS`. Il faut rediriger vers un `single host` vers l'IP du Windows Server et le port `HTTPS`. Il faut faire de même avec le port 3389.

Comme nous avons mis en place le service Netdata, il faut faire une redirection du port 19999 vers l'IP du serveur Samba et le port 19999.

Les règles dans le pare-feu sont créées automatiquement.

### Portail captif

Avant toute chose, il faut paramétrer le service annuaire AD sur le pfsense. Pour cela, Il suffit d'aller dans la partie `system`, `user manager`. Dans l'onglet `groups`, on ajoute un groupe de type `remote`, on assigne le privilège permettant l'authentification par le portail captif. Il faut ensuite aller dans l'onglet `authentication servers` et ajouter le Windows Server. Le type est `LDAP`, on définit l'ip du serveur, le port sera 389 en TCP. La recherche sera sur l'arbre entier. On remplit d'abord la partie `Bind credentials` avec la ligne `CN=bind,CN=Users,DC=ynov,DC=shop` et on complète avec le mot de passe correspondant à l'utilisateur créé au préalable sur le Windows server. Ensuite, on clique sur le bouton "Select a container" et cela permet automatiquement de remplir les informations pour accéder à l'annuaire. On laisse les autres informations par défaut et on valide la création.

Nous pouvons donc créer le portail captif. Pour cela, il faut se rendre dans `services`, `captive portal`.
On en ajoute un sur l'interafce LAN. On y définit les temps avant la déconnexion automatique nécessitant une reconnexion. Nous avons redirigé une fois la connexion faite vers la page web permettant d'accéder aux RemoteApp. Nous avons également autorisé plusieurs connexions d'un même utilisateur. Nous pouvons également définir une image personnalisé pour le portail captif, que ce soit le logo ou l'image de fond. Il faut choisir une authentification avec la méthode back end et choisir le serveur d'authentification du Windows server, créé précédemment sur le PfSense. Il est également nécessaire de paramétrer dans l'onglet `Allowed IP Addresses` les IP des serveurs afin qu'ils puissent accéder au réseau sans s'identifier.

### Serveur DHCP

Nous avons également paramétré le service `DHCP Server`. Nous avons chosi de faire démarrer la plage IP en `.100` jusqu'en `.199` afin de laiser 100 clients se connecter via DHCP. Le serveurs DNS à mettre est l'IP du windows server afin que le PC client puisse accéder aux services du WIndows server (AD). Il est également préférable de rajouter un second serveur DNS, ici celui de google `8.8.8.8`. Nous avons mis l'IP de la gateway, donc celle du PfSense et nous avons complété le nom de domaine.
