$name= Read-Host -Prompt "enter username"
Get-ChildItem C:\MicroSIP-3.20.7.zip | Expand-Archive -DestinationPath C:\Users\$name\voip
New-RDRemoteApp -CollectionName "QuickSessionCollection" -DisplayName "VOIP" -FilePath "C:\Users\$name\voip\microsip.exe" -UserGroups $name -Alias "$name _voip"
