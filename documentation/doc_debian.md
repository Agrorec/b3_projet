# Debian

## Diverses configurations

### Samba

Pour installer le serveur Samba sur Debian, il faut taper la commande `apt update` pour d'abord mettre à jour les repos et ensuite `apt install samba` pour installer le service Samba. Une fois l'installation effectué, il est nécessaire de l'adapter à notre cas d'utilisation. Nous avons donc rajouté en fin de fichier les informations relatives à notre usage :

```bash
[Public]
comment = Public folder
path = /samba/public
read only = no
browsable = yes
guest ok = yes

[Clement]
comment = Clément's folder
path = /samba/c.dournet
read only = no
browsable = yes
guest ok = no
valid users = c.dournet

[Valentin]
comment = Valentin's folder
path = /samba/v.lachamp
read only = no
browsable = yes
guest ok = no
valid user = v.lachamp

[Sacha]
comment = Sacha's folder
path = /samba/s.vilallonga
read only = no
browsable = yes
guest ok = no
valid user = s.vilallonga
```  

On y définit les propriétés de chaque dossier partagé : le nom, le commentaire, le chemin d'accès sur le disque, s'il est ouvert en écriture et les utilisateurs autorisés à y accéder. Pour créer un utilisateur, il faut d'abord qu'il soit créé sur la machine hôte, puis il faut le créer pour Samba avec la commande `smbpasswd -a [username]`.

### Netdata

Pour installer Netdata sur Debian, il faut taper la commande `apt install netdata`. Il faut ensuite taper la commande `systemctl start netdata` pour lancer le service. Le service est désormais accessible sur un navigateur via l'ip et le port 19999. Nous pouvons accéder à toutes les métriques.
