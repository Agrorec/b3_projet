# Windows Server

## Diverses configurations

### Service Active Directory

Il faut d'abord commencer par mettre en place le service d'annuaire AD.
Pour cela, soit on passe par l'interface graphique, on coche les services AD et on installe les services,
soit on passe par ce fichier de configuration XML. Pour lancer l'exécution, il suffit d'ouvrir un terminal et de taper la commande
`Install-WindowsFeature –ConfigurationFilePath [path]`.

<a href="files/ad.xml">Modèle XML</a>

Une fois l'installation effectuée, il faut configurer le service. Là aussi deux possibilités :
passer par l'interface graphique et remplir les informations demandées, soit utiliser ce script powershell qui demandera uniquement un mot de passe administrateur pour l'annuaire et la suite de l'installation est automatisée.

<a href="files/ynov.ps1">Script PowerShell AD</a>

### Service RDS

Une fois les services annauire AD installés, nous pouvons désormais procéder à l'installation des services RDS. Pour cela, il faut ouvrir le gestionnaire de serveur, cliquer sur ajouter des rôles et des fonctionnalités, choisir l'option d'installation des services bureau à distance, choisir l'option déploiement rapide, l'option déploiement de bureau basée sur une session et laisser l'installation s'effectuer.

Pour que les services fonctionnent, il faut au minimum créer un certificat. Pour cela, il faut ouvrir le gestionnaire de serveur, aller dans les services bureau à distance, onglet collections. Dans la partie supérieure, cliquer sur tâches et modifier les propriétés de déploiement. Aller dans l'onglet certificats et créer un certificat pour le rôle d'accès web aux services bureau à distance. Il est important de cocher la case autorisant l'ajout de ce certificat sur les ordinateurs de destination.

### RemoteApp

Une fois le certificat créé, la page web est accessible soit par le biais de l'adresse IP, soit par le nom d'hôte de la machine. Afin d'ajouter de nouvelles RemoteApp, nous pouvons passer par ce script qui va permettre de déployer les fichiers du programme de voIP dans le dossier utilisateur de la paersonne et de créer la RemoteApp sur l'interface web. Le script va demander le nom d'utilisateur, donc le nom permettant la connexion par l'annauire et l'app sera déployée automatiquement. Pour que le script fonctionne, il faut mettre le client téléchargé en version portable sous format zip à la racine. Dans notre cas, il s'agit de la version 3.20.7.

<a href="files/voip.ps1">Script PowerShell VoIP</a>

Pour accéder à l'interface, elle soit accessible par l'IP du serveur et `/rdweb` dans le LAN, soit via le nom de domaine `extranet.ynov.shop/rdweb`. 

### Portail captif

Pour permettre l'authentification par le portail captif, ilfaut créer un administrateur et un utilisateur bind pour la recherche d'utilisateur dans la base par le portail captif. On leur assigne le rôle CaptivePortal créé au préalable. Cette configuration s'effectue en passant par le gestionnaire de serveur, onglet `outils` et `utilisateurs et ordinateurs Active Directory`.

