# Projet fin d'année B3

par Clément Dournet, Sacha Villalonga et Valentin Lachamp

## Description du projet

Notre projet s'articule autour de la **VOIP**, nous allons d'abord mettre en place un serveur, sur lequel fonctionnera le **Asteriks** combiné avec un annuaire **Active Directory**.

Par la suite nous scinderons le projet en une partie infra et une partie sécurité. Lors de cette phase l'objectif sera de trouver des vulnérabilités et si possible de les corriger.

## Présentation du réseau

Notre réseau sera composé de plusieurs éléments :

- Un premier serveur contenant un service Active Directory et un intranet 
- Un deuxième serveur contenant le service Asterisk (téléphone VOIP)
- Un troisième serveur contenant un serveur Samba et un système de monitoring
- Un pare-feu Pfsense

![](pictures/network.png)
